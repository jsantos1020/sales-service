package com.estudos.sale.config.kafka

import com.estudos.sale.adapters.out.message.SaleMessage
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Serializer

class CustomSerializer : Serializer<SaleMessage?> {
    private val objectMapper = ObjectMapper()
    override fun serialize(s: String?, saleMessage: SaleMessage?): ByteArray? {
        return try {
            if (saleMessage == null) {
                null
            } else objectMapper.writeValueAsBytes(saleMessage)
        } catch (e: Exception) {
            throw SerializationException("Error when serializing SaleMessage to byte[]")
        }
    }
}