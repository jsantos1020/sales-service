package com.estudos.invertory.config.kafka

import com.estudos.invertory.adapters.out.message.SaleMessage
import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.Deserializer


class CustomDeserializer : Deserializer<SaleMessage?> {
    private val objectMapper = ObjectMapper()
    override fun deserialize(topic: String?, data: ByteArray?): SaleMessage? {
        return try {
            if (data == null) {
                null
            } else objectMapper.readValue<SaleMessage>(
                String(data, charset("UTF-8")),
                SaleMessage::class.java
            )
        } catch (e: Exception) {
            throw SerializationException("Error when deserializing byte[] to SaleMessage")
        }
    }
}