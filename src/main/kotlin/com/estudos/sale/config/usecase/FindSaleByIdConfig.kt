package com.estudos.sale.config.usecase

import com.estudos.sale.adapters.out.FindSaleByIdAdapter
import com.estudos.sale.application.core.usecase.FindSaleByIdUseCase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FindSaleByIdConfig {

    @Bean
    fun findSaleByIdUseCase(findSaleByIdAdapter: FindSaleByIdAdapter) =
        FindSaleByIdUseCase(findSaleByIdOutputPort = findSaleByIdAdapter)
}
