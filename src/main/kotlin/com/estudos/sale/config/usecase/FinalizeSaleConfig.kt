package com.estudos.sale.config.usecase

import com.estudos.sale.adapters.out.SaveSaleAdapter
import com.estudos.sale.application.core.usecase.FinalizeSaleUseCase
import com.estudos.sale.application.core.usecase.FindSaleByIdUseCase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FinalizeSaleConfig {

    @Bean
    fun finalizeSaleUseCase(findSaleByIdUseCase: FindSaleByIdUseCase, saveSaleAdapter: SaveSaleAdapter) =
        FinalizeSaleUseCase(findSaleByIdInputPort = findSaleByIdUseCase, saveSaleOutputPort = saveSaleAdapter)
}
