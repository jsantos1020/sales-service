package com.estudos.sale.config.usecase

import com.estudos.sale.adapters.out.SaveSaleAdapter
import com.estudos.sale.adapters.out.SendCreatedSaleAdapter
import com.estudos.sale.application.core.usecase.CreateSaleUseCase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class CreateSaleConfig {

    @Bean
    fun createSaleUseCase(saveSaleAdapter: SaveSaleAdapter, sendCreatedSaleAdapter: SendCreatedSaleAdapter) =
        CreateSaleUseCase(saveSaleAdapter, sendCreatedSaleAdapter)
}