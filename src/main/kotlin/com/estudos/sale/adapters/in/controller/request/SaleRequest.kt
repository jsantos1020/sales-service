package com.estudos.sale.adapters.`in`.controller.request

import java.math.BigDecimal

data class SaleRequest(
    val userId: Int,
    val productId: Int,
    val quantity: Int,
    val amount: BigDecimal
)
