package com.estudos.sale.adapters.`in`.consumer

import com.estudos.sale.adapters.out.message.SaleMessage
import com.estudos.sale.application.core.domain.enums.SaleEvent
import com.estudos.sale.application.ports.`in`.FinalizeSaleInputPort
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class ReceiveSaleToFinalizeConsumer(private val finalizeSaleInputPort: FinalizeSaleInputPort) {

    @KafkaListener(topics = ["tp-saga-sale"], groupId = "sale-finalize")
    fun receive(saleMessage: SaleMessage) {
        if (SaleEvent.VALIDATED_PAYMENT == saleMessage.event) {
            finalizeSaleInputPort.finalize(sale = saleMessage.sale)
        }
    }
}
