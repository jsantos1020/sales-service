package com.estudos.sale.adapters.`in`.controller.mapper

import com.estudos.sale.adapters.`in`.controller.request.SaleRequest
import com.estudos.sale.application.core.domain.Sale

fun SaleRequest.toSale() = Sale(
    userId = this.userId,
    productId = this.productId,
    quantity = this.quantity,
    amount = this.amount,
)