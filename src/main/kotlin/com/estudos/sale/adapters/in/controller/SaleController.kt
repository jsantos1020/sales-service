package com.estudos.sale.adapters.`in`.controller

import com.estudos.sale.adapters.`in`.controller.mapper.toSale
import com.estudos.sale.adapters.`in`.controller.request.SaleRequest
import com.estudos.sale.application.ports.`in`.CreateSaleInputPort
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/sale")
class SaleController(private val createSaleInputPort: CreateSaleInputPort) {

    private val logger = KotlinLogging.logger {}

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createSale(@RequestBody request: SaleRequest) {
        logger.info { "Creating a sale..." }
        createSaleInputPort.create(request.toSale())
    }
}
