package com.estudos.sale.adapters.out

import com.estudos.sale.adapters.out.repository.SaleRepository
import com.estudos.sale.adapters.out.repository.mapper.toEntity
import com.estudos.sale.adapters.out.repository.mapper.toSale
import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.ports.out.SaveSaleOutputPort
import org.springframework.stereotype.Component

@Component
class SaveSaleAdapter(
    private val saleRepository: SaleRepository
) : SaveSaleOutputPort {

    override fun save(sale: Sale): Sale {
        val saleEntityCreated = saleRepository.save(sale.toEntity())
        return saleEntityCreated.toSale()
    }
}