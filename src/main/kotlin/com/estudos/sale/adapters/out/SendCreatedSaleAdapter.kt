package com.estudos.sale.adapters.out

import com.estudos.sale.adapters.out.message.SaleMessage
import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.core.domain.enums.SaleEvent
import com.estudos.sale.application.ports.out.SendCreatedSaleOutputPort
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class SendCreatedSaleAdapter(
    private val kafkaTemplate: KafkaTemplate<String, SaleMessage>
) : SendCreatedSaleOutputPort {
    override fun send(sale: Sale, event: SaleEvent) {
        val message = SaleMessage(sale, event)
        kafkaTemplate.send("tp-saga-sale", message)
    }
}