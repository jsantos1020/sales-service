package com.estudos.sale.adapters.out

import com.estudos.sale.adapters.out.repository.SaleRepository
import com.estudos.sale.adapters.out.repository.mapper.toSale
import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.ports.out.FindSaleByIdOutputPort
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import java.util.UUID

@Component
class FindSaleByIdAdapter(private val saleRepository: SaleRepository) : FindSaleByIdOutputPort {
    override fun find(id: UUID): Sale? {
        return saleRepository.findByIdOrNull(id = id)?.toSale()
    }
}
