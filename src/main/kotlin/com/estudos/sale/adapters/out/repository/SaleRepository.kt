package com.estudos.sale.adapters.out.repository

import com.estudos.sale.adapters.out.repository.entity.SaleEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface SaleRepository : JpaRepository<SaleEntity, UUID>