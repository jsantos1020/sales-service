package com.estudos.sale.adapters.out.repository.entity

import jakarta.persistence.Entity
import jakarta.persistence.Id
import java.math.BigDecimal
import java.util.UUID

@Entity(name = "sales")
data class SaleEntity(

    @Id
    val id: UUID,
    val productId: Int,
    val userId: Int,
    val amount: BigDecimal,
    val statusId: Int,
    val quantity: Int
)
