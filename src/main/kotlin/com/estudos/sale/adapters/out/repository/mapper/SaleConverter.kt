package com.estudos.sale.adapters.out.repository.mapper

import com.estudos.sale.adapters.out.repository.entity.SaleEntity
import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.core.domain.SalesStatus

fun Sale.toEntity() = SaleEntity(
    id = this.id,
    productId = this.productId,
    userId = this.userId,
    amount = this.amount,
    statusId = this.status.statusId,
    quantity = this.quantity
)

fun SaleEntity.toSale(): Sale {
    val status = SalesStatus from this.statusId
    return Sale(
        id = this.id,
        productId = this.productId,
        userId = this.userId,
        amount = this.amount,
        status = status!!,
        quantity = this.quantity
    )
}