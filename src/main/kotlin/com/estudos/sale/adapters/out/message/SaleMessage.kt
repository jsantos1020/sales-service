package com.estudos.sale.adapters.out.message

import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.core.domain.enums.SaleEvent

data class SaleMessage(
    val sale: Sale,
    val event: SaleEvent
)
