package com.estudos.sale.application.core.usecase

import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.ports.`in`.FinalizeSaleInputPort
import com.estudos.sale.application.ports.`in`.FindSaleByIdInputPort
import com.estudos.sale.application.ports.out.SaveSaleOutputPort

class FinalizeSaleUseCase(
    private val findSaleByIdInputPort: FindSaleByIdInputPort,
    private val saveSaleOutputPort: SaveSaleOutputPort,
) : FinalizeSaleInputPort {

    override fun finalize(sale: Sale) {
        val saleResponse = findSaleByIdInputPort.find(id = sale.id)
        saleResponse.saleFinalized()

        saveSaleOutputPort.save(sale = saleResponse)
    }
}
