package com.estudos.sale.application.core.usecase

import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.ports.`in`.FindSaleByIdInputPort
import com.estudos.sale.application.ports.out.FindSaleByIdOutputPort
import java.util.UUID

class FindSaleByIdUseCase(private val findSaleByIdOutputPort: FindSaleByIdOutputPort) : FindSaleByIdInputPort {

    override fun find(id: UUID): Sale {
        return findSaleByIdOutputPort.find(id = id) ?: error("Venda não encontrada!")
    }
}
