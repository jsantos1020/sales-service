package com.estudos.sale.application.core.domain

import java.math.BigDecimal
import java.util.UUID

data class Sale(
    val id: UUID = UUID.randomUUID(),
    val productId: Int,
    val userId: Int,
    val amount: BigDecimal,
    val status: SalesStatus = SalesStatus.NONE,
    val quantity: Int
) {
    fun salePending() = this.copy(status = SalesStatus.PENDING)
    fun saleFinalized() = this.copy(status = SalesStatus.FINALIZED)
}

enum class SalesStatus(val statusId: Int) {
    NONE(0),
    PENDING(1),
    FINALIZED(2),
    CANCELED(3);

    companion object {
        private val map = SalesStatus.values().associateBy { it.statusId }
        infix fun from(value: Int) = map[value]
    }
}