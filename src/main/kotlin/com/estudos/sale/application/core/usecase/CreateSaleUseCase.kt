package com.estudos.sale.application.core.usecase

import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.core.domain.enums.SaleEvent.CREATED_SALE
import com.estudos.sale.application.ports.`in`.CreateSaleInputPort
import com.estudos.sale.application.ports.out.SaveSaleOutputPort
import com.estudos.sale.application.ports.out.SendCreatedSaleOutputPort

class CreateSaleUseCase(
    private val saveSaleOutputPort: SaveSaleOutputPort,
    private val sendCreatedSaleOutputPort: SendCreatedSaleOutputPort
) : CreateSaleInputPort {

    override fun create(sale: Sale) {
        sale.salePending()
        saveSaleOutputPort.save(sale = sale).also { saleCreated ->
            sendCreatedSaleOutputPort.send(sale = saleCreated, event = CREATED_SALE)
        }
    }
}