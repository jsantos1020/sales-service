package com.estudos.sale.application.ports.out

import com.estudos.sale.application.core.domain.Sale
import java.util.UUID

interface FindSaleByIdOutputPort {

    fun find(id: UUID): Sale?
}
