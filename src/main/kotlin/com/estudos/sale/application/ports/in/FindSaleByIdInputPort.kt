package com.estudos.sale.application.ports.`in` // ktlint-disable package-name

import com.estudos.sale.application.core.domain.Sale
import java.util.UUID

interface FindSaleByIdInputPort {
    fun find(id: UUID): Sale
}
