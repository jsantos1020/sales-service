package com.estudos.sale.application.ports.out

import com.estudos.sale.application.core.domain.Sale

interface SaveSaleOutputPort {

    fun save(sale: Sale): Sale
}