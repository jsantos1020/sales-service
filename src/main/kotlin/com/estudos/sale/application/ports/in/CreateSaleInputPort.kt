package com.estudos.sale.application.ports.`in`

import com.estudos.sale.application.core.domain.Sale

interface CreateSaleInputPort {

    fun create(sale: Sale)
}