package com.estudos.sale.application.ports.out

import com.estudos.sale.application.core.domain.Sale
import com.estudos.sale.application.core.domain.enums.SaleEvent

interface SendCreatedSaleOutputPort {

    fun send(sale: Sale, event: SaleEvent)
}