package com.estudos.sale

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SaleServiceApplication

fun main(args: Array<String>) {
	runApplication<SaleServiceApplication>(*args)
}
